#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#define PORT 49153
//Worked with Maya, Ben, Blake, Riley and Jason
int main(){
  char buffer[80];
  char message[80];
  int x,y,z = 0;
  //create a socket
  int network_socket;
  network_socket = socket(AF_INET, SOCK_STREAM, 0);

  //address for network_socket
  struct sockaddr_in address;
  memset(&address, '0',sizeof(address));
  address.sin_family = AF_INET;
  address.sin_port = htons(PORT);

  //if statements drawn from https://www.geeksforgeeks.org/socket-programming-cc/
  //Testing to make sure it fails
  //10.115.20.250,pilot.westmont.edu
  if( inet_pton(AF_INET, "10.115.20.250", &address.sin_addr)<=0)
    {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  //printf("check 1 passed\n");
  int check = connect(network_socket, (struct sockaddr *)&address, sizeof(address));
  if (check< 0)
    {
        printf("\nConnection Failed %d \n",check);
        return -1;
    }
  //printf("check 2 passed\n");
  read(network_socket,buffer,sizeof(buffer));
  printf("\nRecieved %s \n please enter username \n",buffer);
  //username
  fgets(message,sizeof(message),stdin);
  printf("\nusername entered: %s\n",message);
  send(network_socket, message, sizeof(message), 0);
  //get first messages
/*  memset(buffer,0,sizeof(buffer));
  read(network_socket,buffer,sizeof(buffer));

  printf("\nRecieved %s \n",buffer);
*/  //start the loop for chat
  while(1){
    //clean the buffer and message to not stack messages, set t and i to 0 to not get unwanted content
    bzero(&message,sizeof(message));
//    memset(message,'\0',sizeof(message));
    //get the message from the user
    printf("\nenter message\n");
    fgets(message,sizeof(message),stdin);
    printf("you input: %s\n", message);
    //read and display messages
    read(network_socket,buffer,sizeof(buffer));
    printf("\nRecieved:\n %s \n",buffer);
    //clean buffer
    bzero(&buffer,sizeof(buffer));
    //memset(buffer, '\0',sizeof(buffer));
    //send message to server
    send(network_socket,message,sizeof(message),0);
  }
  return 0;
}
